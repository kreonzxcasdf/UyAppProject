﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UyApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MessagePage : ContentPage
    {
        public MessagePage()
        {
            InitializeComponent();
        }
        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MessagePage());
        }

        private void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {

        }
        private void TapGestureRecognizer_Tapped_2(object sender, EventArgs e)
        {

        }
        private void TapGestureRecognizer_Tapped_3(object sender, EventArgs e)
        {

        }
        private void TapGestureRecognizer_Tapped_4(object sender, EventArgs e)
        {

        }
    }
}