﻿using CustomRenderer.Android;
using UyApp;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryLogin), typeof(Android_EntryLogin))]
namespace CustomRenderer.Android
{
    class Android_EntryLogin : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Background = null;
                
            }
        }
    }
}